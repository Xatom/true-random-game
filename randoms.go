package main

const CacheSize = 256

type Randoms struct {
	Data []int
}

func NewRandoms() *Randoms {
	return &Randoms{}
}

func (r *Randoms) IsEmpty() bool {
	return len(r.Data) == 0
}

func (r *Randoms) Populate(api *Api, min int, max int) error {
	res, err := api.GenerateIntegers(CacheSize, min, max)

	if err == nil {
		r.Data = res.Random.Data
	}

	return err
}

func (r *Randoms) Pop() int {
	id := r.Data[0]

	r.Data = r.Data[1:]

	return id
}
