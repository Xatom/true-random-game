FROM golang:alpine AS build

WORKDIR /go/src/trg
COPY . .

RUN apk add --no-cache git yarn
RUN yarn install
RUN yarn run webpack --config webpack.config.js --mode production
RUN go get github.com/ybbus/jsonrpc
RUN go build

FROM alpine:latest

WORKDIR /app
COPY --from=build /go/src/trg/trg trg
COPY --from=build /go/src/trg/static static
COPY --from=build /go/src/trg/templates templates

RUN apk add --no-cache ca-certificates

ARG version=Test

ENV VERSION=${version}

EXPOSE 80

ENTRYPOINT ["./trg"]
