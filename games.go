package main

type Games struct {
	Data []string
}

func NewGames(games ...string) *Games {
	return &Games{
		Data: games,
	}
}

func (g *Games) ById(id int) string {
	if id > len(g.Data)-1 {
		return "Unknown Game"
	}

	return g.Data[id]
}
