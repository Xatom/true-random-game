package main

import (
	"net/http"
	"log"
	"os"
	"sync"
	"path/filepath"
	"html/template"
	"fmt"
)

var ApiClient = NewApi("https://api.random.org/json-rpc/1/invoke", os.Getenv("RANDOM_ORG_API_KEY"))
var GamesHelper = NewGames("Team Fortress 2", "Half-Life 2", "Portal 2", "Civilization V")
var RandomsCache = NewRandoms()

var Mutex = sync.Mutex{}

var HomeTemplate = parseTemplate("home.gohtml")
var AboutTemplate = parseTemplate("about.gohtml")

type HomeModel struct {
	Games        []string
	GameIds      map[int]bool
	RandomGame   string
	CurrentCache int
	MaxCache     int
}

type AboutModel struct {
	Version string
}

func (h *HomeModel) CacheProgress() float64 {
	return float64(h.CurrentCache) / float64(h.MaxCache) * 100
}

func main() {
	ApiClient.EnforceKey()

	fs := http.FileServer(http.Dir("static"))

	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.HandleFunc("/", handleHome)
	http.HandleFunc("/about", handleAbout)

	log.Fatal(http.ListenAndServe(":80", nil))
}

func parseTemplate(name string) *template.Template {
	lp := filepath.Join("templates", "layout.gohtml")
	np := filepath.Join("templates", name)

	tp, err := template.ParseFiles(lp, np)

	if err != nil {
		log.Fatalf("Error parsing template %s: %v", name, err)
	}

	return tp
}

func handleHome(w http.ResponseWriter, r *http.Request) {
	ids := map[int]bool{}

	r.ParseForm()

	for i, _ := range GamesHelper.Data {
		if r.Method != http.MethodPost || r.Form.Get(fmt.Sprintf("game-%d", i)) != "" {
			ids[i] = true
		}
	}

	trg := randomGame(ids)
	err := HomeTemplate.ExecuteTemplate(w, "layout", trg)

	if err != nil {
		log.Printf("Error executing home: %v", err)
	}
}

func handleAbout(w http.ResponseWriter, r *http.Request) {
	version := os.Getenv("VERSION")

	if version == "" {
		version = "Development"
	}

	err := AboutTemplate.ExecuteTemplate(w, "layout", AboutModel{
		Version: version,
	})

	if err != nil {
		log.Printf("Error executing about: %v")
	}
}

func randomGame(ids map[int]bool) *HomeModel {
	if len(ids) < 2 {
		return nil
	}

	Mutex.Lock()
	defer Mutex.Unlock()

	for {
		if RandomsCache.IsEmpty() {
			err := RandomsCache.Populate(ApiClient, 0, len(GamesHelper.Data)-1)

			if err != nil {
				log.Printf("Random.org unavailable: %v", err)
				return nil
			}
		}

		id := RandomsCache.Pop()

		if !ids[id] {
			continue
		}

		return &HomeModel{
			Games:        GamesHelper.Data,
			GameIds:      ids,
			RandomGame:   GamesHelper.ById(id),
			CurrentCache: len(RandomsCache.Data),
			MaxCache:     CacheSize,
		}
	}
}
