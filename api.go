package main

import (
	"github.com/ybbus/jsonrpc"
	"log"
)

type Api struct {
	RpcClient jsonrpc.RPCClient
	Key       string
}

type ApiIntegersRequest struct {
	ApiKey string `json:"apiKey"`
	N      int    `json:"n"`
	Min    int    `json:"min"`
	Max    int    `json:"max"`
}

type ApiIntegersResponse struct {
	Random        ApiRandomResponse `json:"random"`
	BitsUsed      int               `json:"bitsUsed"`
	BitsLeft      int               `json:"bitsLeft"`
	RequestsLeft  int               `json:"requestsLeft"`
	AdvisoryDelay int               `json:"advisoryDelay"`
}

type ApiRandomResponse struct {
	Data           []int  `json:"data"`
	CompletionTime string `json:"completionTime"`
}

func NewApi(endpoint string, key string) *Api {
	return &Api{
		RpcClient: jsonrpc.NewClient(endpoint),
		Key:       key,
	}
}

func (a *Api) GenerateIntegers(n int, min int, max int) (*ApiIntegersResponse, error) {
	var res *ApiIntegersResponse

	err := a.RpcClient.CallFor(&res, "generateIntegers", &ApiIntegersRequest{
		ApiKey: a.Key,
		N:      n,
		Min:    min,
		Max:    max,
	})

	return res, err
}

func (a *Api) EnforceKey() {
	if a.Key == "" {
		log.Fatal("No Random.org API key set")
	}
}
